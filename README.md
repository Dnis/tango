# tangoa
## Introduction
`tangoa` is a Python library implementing an outer approximation approach using 
projective cutting planes. The package can be used to solve quadratic programs 
with indicator constraints exactly. The approach is based on the article
[Projective Cutting Planes for General QP with Indicator Constraints](http://www.optimization-online.org/DB_HTML/2021/08/8547.html):
> General quadratic optimization problems with linear constraints and additional indicator constraints on the variables are studied. Based on the well-known perspective reformulation for mixed-integer quadratic optimization problems, projective cuts are introduced as new valid inequalities for the general problem. The key idea behind the theory of these cutting planes is the projection of the continuous variables onto the space of optimal solutions dependent on the choice of indicator variables. The advantages of projective cutting planes for practical computations are illustrated with a numerical study of uncapacitated facility location problems.

## Dependenices
The library requires the MIP solver `Gurobi` with its Python interface `gurobipy`.
Furthermore, the Python packages `numpy`, `scipy`, and `threadpoolctl` are required.
## Installation
### From pypi
Execute 
    
    pip3 install --user tangoa
    
in a terminal.

### From source
Navigate into the root folder of `tangoa` and call

    python3 setup.py install --user
    
### Computational experiments
See [here](experiments/README.md) for how to conduct the computational experiments
from the article 
[Projective Cutting Planes for General QP with Indicator Constraints](http://www.optimization-online.org/DB_HTML/2021/08/8547.html).