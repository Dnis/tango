import sys
sys.path.insert(1, '../..')

from lab import lab

if __name__ == '__main__':
    lab.ecdf_plot("door"). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("node_count", "Number of nodes"). \
        add_unit_to_compare("Outer Approximation", method="oa"). \
        add_unit_to_compare("Perspective Reformulation", method="perspective"). \
        filter_data(sub_experiment="1"). \
        set_yaxis_label("Proportion of solved instances"). \
        add_grid("n_facilities", "n_customers", col_label="# of fac.", row_label="# of cust."). \
        set_font_scale(1). \
        set_line_width(2). \
        set_save_fig_size("a3"). \
        save("nodes_small.pdf")

    lab.ecdf_plot("door"). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("node_count", "Number of nodes"). \
        add_unit_to_compare("Logical constraints", method="logical"). \
        filter_data(sub_experiment="1", n_facilities=30, n_customers=150). \
        set_yaxis_label("Proportion of solved instances"). \
        set_font_scale(1). \
        set_save_fig_size("a3"). \
        set_line_width(2). \
        save("nodes_logical_small.pdf")

    lab.ecdf_plot("door"). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("node_count", "Number of nodes"). \
        add_unit_to_compare("Outer Approximation", method="oa"). \
        add_unit_to_compare("Perspective Reformulation", method="perspective"). \
        filter_data(sub_experiment="big"). \
        set_yaxis_label("Proportion of solved instances"). \
        add_grid("n_customers", col_label="# of cust."). \
        set_font_scale(1). \
        set_save_fig_size("a3"). \
        set_line_width(2). \
        save("nodes_big.pdf")
