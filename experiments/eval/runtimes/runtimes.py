import sys

sys.path.insert(1, '../..')

from lab import lab

if __name__ == '__main__':
    lab.ecdf_plot("door"). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("complete_runtime", "Run time in seconds"). \
        add_unit_to_compare("Outer Approximation", method="oa"). \
        add_unit_to_compare("Perspective Reformulation", method="perspective"). \
        filter_data(sub_experiment="1"). \
        add_grid("n_facilities", "n_customers", col_label="# of fac.", row_label="# of cust."). \
        set_yaxis_label("Proportion of solved instances"). \
        set_font_scale(0.9). \
        set_save_fig_size("a3"). \
        set_line_width(2). \
        save("squfl_runtimes_small.pdf")

    lab.ecdf_plot("door"). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("complete_runtime", "Run time in seconds"). \
        add_unit_to_compare("Logical constraints", method="logical"). \
        filter_data(sub_experiment="1"). \
        add_grid("n_facilities", "n_customers", col_label="# of fac.", row_label="# of cust."). \
        set_yaxis_label("Proportion of solved instances"). \
        set_font_scale(0.9). \
        set_save_fig_size("a3"). \
        set_line_width(2). \
        save("squfl_runtimes_logical_small.pdf")

    lab.ecdf_plot("door"). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("complete_runtime", "Run time in seconds"). \
        add_unit_to_compare("Outer Approximation", method="oa"). \
        add_unit_to_compare("Perspective Reformulation", method="perspective"). \
        filter_data(sub_experiment="big"). \
        set_yaxis_label("Proportion of solved instances"). \
        add_grid("n_customers", col_label="# of cust."). \
        set_font_scale(0.9). \
        set_save_fig_size("a3"). \
        set_line_width(2). \
        save("squfl_runtimes_big.pdf")
