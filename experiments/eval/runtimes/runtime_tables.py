import sys
sys.path.insert(1, '../..')

from lab import lab
import pandas as pd

if __name__ == '__main__':
    table = lab.mean_table("door"). \
        set_methods_key("method", label="Method"). \
        set_value_of_interest("complete_runtime"). \
        add_unit_to_compare("Outer Approximation", method="oa"). \
        add_unit_to_compare("Perspective Reformulation", method="perspective"). \
        add_unit_to_compare("Logical Formulation", method="logical"). \
        set_instance_keys(("n_facilities", "n_customers")). \
        build_table()
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        print(table)