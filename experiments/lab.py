import json

import pathlib

from packaging import version
import digitallab

assert version.parse(digitallab.__version__) >= version.parse("1.4.3.1"), \
    "You are using an incompatible version of digitallab. " \
    "Please install digitallab 1.4.3.1 by calling 'pip install --user digitallab==1.4.3.1'."

from digitallab.lab import Lab

this_path = str(pathlib.Path(__file__).parent.resolve())

with open(this_path + "/mongodb_credentials.json", "r") as file:
    mongodb_credentials = json.load(file)

mongodb_url = 'mongodb://'
mongodb_url += mongodb_credentials["user_name"]
if mongodb_credentials["password"]:
    mongodb_url += ":" + mongodb_credentials["password"]
mongodb_url += "@" + mongodb_credentials["server"]
mongodb_url += "/" + mongodb_credentials["database"]
if mongodb_credentials["auth_mechanism"]:
    mongodb_url += "?authMechanism=" + mongodb_credentials["auth_mechanism"]

lab = Lab("squfl_outer_approx").add_mongodb_storage(mongodb_url, mongodb_credentials["database"])
lab.set_keys_for_evaluation(this_path + "/eval/required_keys.json")
# lab.force_use_of_cache(True)
