# Instructions for running the computational experiments
We apply our method to the so called [separable quadratic 
uncapacitated facility location problem](https://link.springer.com/chapter/10.1007/978-1-4614-1927-3_3). Given a set of 
customers and a set of facilities, the objective 
of the problem is to decide which facilities to open, and if, 
how much they should supply individual customers. 

We are comparing the outer approximation approach implemented in `tangoa`, 
the perspective reformulation, and a formulation with logical constraints.

## Requirements
Install the following software for running the experiments.
### Python packages
* tangoa
* [digitallab](https://gitlab.com/Dnis/dlab) >= 1.4.3.2

### Database
* MongoDB

## Instructions
##### 1. MongoDB config
First create a file `mongodb_credentials.json` in this directory (`tango/experiments`). Then copy-paste

    {
        "user_name": "<YOUR MONGODB USER NAME>",
        "password": "<YOUR MONGODB PASSOWRD>",
        "database": "<DATABASE WHERE RESULTS ARE STORED>",
        "server": "<SERVER ADDRESS OF MONGODB>",
        "auth_mechanism": "SCRAM-SHA-1"
    }

into the file and swap the term `<...>` with your credentials for your MongoDB.

##### 2. Conduct the experiments
For running the experiments execute

    python3 run.py
    
##### 3. Evaluating the experiments
Run the following Python scripts to produce the tables and plots:
* `eval/nodes/nodes.py` for producing the number of nodes plots.
* `eval/runtimes/runtimes.py` for producing the runtime plots.
* `eval/runtimes/runtime_table.py` for producing the table including all 
mean runtimes.