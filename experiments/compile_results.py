from digitallab.helper.conversion import numpy_dict_to_json


def compile_results(z, x, objval, model, solver):
    gap = model.getAttr("MIPGap")
    time = model.getAttr("Runtime")
    status = model.getAttr("Status")
    node_count = model.getAttr("NodeCount")

    results = dict()

    results["z"] = z
    results["x"] = x
    results["objective_value"] = objval

    results["gap"] = gap
    results["runtime"] = time
    results["complete_runtime"] = solver.complete_runtime
    results["gurobi_status"] = status
    results["node_count"] = node_count

    return numpy_dict_to_json(results)
