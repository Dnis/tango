from compile_results import compile_results
from tangoa.SQUFL.instances import SQUFLInstance
from tangoa.SQUFL.squfl import SeparableQuadraticUFL
from tangoa.solvers.indicator_constraints import LogicalSolver
from tangoa.solvers.oa import OASolver
from tangoa.solvers.perspective_solver import PerspectiveSolver

from lab import lab


@lab.experiment
def main(_config):
    essential_keys = ("version", "method", "n_facilities", "n_customers", "seed")
    for k in essential_keys:
        if k not in _config:
            raise KeyError(k + " is missing in the config.")
    squfl_instance = SQUFLInstance(_config["n_facilities"], _config["n_customers"], seed=_config["seed"])

    if _config["method"] == "oa":
        solver = OASolver
    elif _config["method"] == "logical":
        solver = LogicalSolver
    elif _config["method"] == "perspective":
        solver = PerspectiveSolver
    else:
        raise KeyError("Method " + _config["method"] + " does not exist.")
    squfl = SeparableQuadraticUFL(squfl_instance.Q, squfl_instance.c, solver=solver, **_config)
    z, x, objval = squfl.solve()
    m = squfl.solver.model
    ret = compile_results(z, x, objval, m, squfl.solver)
    del squfl
    return ret


lab.run_experiments(main, "experiment_settings", number_of_parallel_runs=1,
                    number_of_query_chunks=100, number_of_query_workers=100, show_selection_dialog=True)
