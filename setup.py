import codecs
import os.path

from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name='tangoa',
    version=get_version("tangoa/__init__.py"),
    packages=find_packages(exclude=("tests")),
    url='https://gitlab.com/Dnis/tango',
    license='MIT',
    author='Dennis Kreber',
    author_email='dnis.kk@gmail.com',
    description="Library for solving quadratic programs with indicator constraints.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["numpy", "scipy", "threadpoolctl"],
    python_requires='>=3.4',
    zip_safe=False
)
